Feature: This feature deals with the journey of booking flights
  Background: The user lands on the required url and wait for it to load completely
    Given I navigate to the main landing page
    And wait for the page to load completely

  Scenario: Check if the trip summary is shown after the sequence of events

    When I change the class of travel
    And I click on the destination of travel field
    And I enter a random location from IATA list
    And  I click on the search button
    Then I click on the first best flight expand button
    And I click on the select flight button
 #   Then assert if the the arrival page is shown
    Then I click on the first returning best flight option
    And assert if the trip summary is shown
