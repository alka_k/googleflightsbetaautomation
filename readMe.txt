# Note: The code is written using Java, Selenium and Cucumber in IntelliJ IDE
1. Before checking out the code, please make sure that the IDE is ready for cucumber syntax. Please intall the plugin "Cucumber for Java" to start.
2. Once the code is in you IDE,
a. To run all the scenarios written in all the feature files, please right click on the features folder (src>test>java>Features)and run all the features
b. If a single scenario has to be run, please go to the feature file (src>test>java>Features>)and right click on the text "scenario" in the file and then run it
c. If an entire feature file has to be run,right click on the text "feature" in the feature file and run it.
3. After the code has executed, the console will show the no of test cases passed and failed