package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class TripSummaryPage {

    public TripSummaryPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[2]/div[3]/div[1]/ol/li[3]/span")
    public WebElement tripSummaryText;

    public String getTripSummaryText(){
        return tripSummaryText.getText();
    }


}
