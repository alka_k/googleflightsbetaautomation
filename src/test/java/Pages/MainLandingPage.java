package Pages;

import Base.Utility;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class MainLandingPage {

    public MainLandingPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/dropdown-menu/div/div[1]/span[1]")
    public WebElement ticketTypeMenu;

    @FindBy(how= How.XPATH, using="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/dropdown-menu/div/div[2]/menu-item[2]")
    public WebElement TicketTypeOneway;

    @FindBy(how= How.XPATH, using="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/dropdown-menu/div/div[2]/menu-item[3]")
    public WebElement TicketTypeMultiCity;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[2]/div[3]")//
    public WebElement destinationTravelFieldBeforeClick;

    @FindBy(how = How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/destination-picker/div[1]/div[2]/div[2]/input")
    public WebElement destinationTravelFieldAfterClick;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[4]/floating-action-button")
    public WebElement searchButton;

    @FindBy(how= How.XPATH, using="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[2]/div[5]/div[1]/div[2]")
    public WebElement journeyStartDate;

    @FindBy(how= How.XPATH, using="/html/body/div[2]/div/div[5]/div/div[5]/div/div[4]/div[2]/div[1]/date-input/input")
    public WebElement enterDepartureDate;

    @FindBy(how= How.XPATH, using="/html/body/div[2]/div/div[5]/div/div[5]/div/div[4]/div[2]/div[3]/date-input")
    public WebElement journeyEndDate;

    @FindBy(how= How.XPATH, using="/html/body/div[2]/div/div[5]/div/div[5]/div/div[4]/div[2]/div[3]/date-input/input")
    public WebElement enterReturnDate;

    @FindBy(how= How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[2]/div[1]/div[3]/span[2]/span/span[2]")
    public WebElement getFromTravelField;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[3]/div/g-raised-button/div[1]")
    public WebElement addFlightBtn;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[1]/div/div[3]")
    public WebElement plusAdultPas;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[2]/div/div[3]")
    public WebElement plusChildrenPas;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[3]/div/div[3]")
    public WebElement plusInfantPas;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[4]/div/div[3]")
    public WebElement plusInfantLapPas;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[2]/div")
    public WebElement noOfPassengersMenu;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[2]/div/div[2]")
    public WebElement noOfChildrenPas;

    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[6]")
    public WebElement errorMessageContainer;

    @FindBy(how=How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[3]/dropdown-menu/div/div[1]")
    public WebElement classForTravelMenu;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[3]/dropdown-menu/div/div[2]/menu-item[3]")
    public WebElement businessClass;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div[5]/g-raised-button/div")
    public WebElement overlayDoneBtn;

    @FindBy(how=How.XPATH, using ="/html/body/div[2]/div/div[5]/div/div[5]/div/div/div[2]/div/div[1]")
    public WebElement minusChildrenPas;

    public boolean isFromTravelFilled(){
        return getFromTravelField.getText().isEmpty();
    }

    public void enterDepartureDate(String dateOfDeparture){
        enterDepartureDate.sendKeys(dateOfDeparture);
        Utility.pressEnterKey();
    }

    public void enterReturnDate(String dateOfReturn){
        journeyEndDate.click();
        enterReturnDate.sendKeys(dateOfReturn);
        Utility.pressEnterKey();
    }

    public Boolean IsJourneyEndDateActive(){
        return Utility.checkIfElementEnabled(journeyEndDate);
    }

    public void selectOneWay() {
        ticketTypeMenu.click();
        TicketTypeOneway.click();
    }

    public void selectMultiCity(){
        ticketTypeMenu.click();
        TicketTypeMultiCity.click();
    }

    public boolean isAddFlightActive(){
        return Utility.checkIfElementEnabled(addFlightBtn);
    }

    public void enterTextDestinationTravel(String Destination){
        destinationTravelFieldAfterClick.sendKeys(Destination);
        Utility.pressEnterKey();
    }

    public void clickDestinationTravel(){
        destinationTravelFieldBeforeClick.click();
    }

    public boolean isSearchBtnActive(){
        return Utility.checkIfElementEnabled(searchButton);
    }

    public void clickOverlayDoneBtn(){
        overlayDoneBtn.click();
    }

    public void clickNoOfPassengersMenu(){
        noOfPassengersMenu.click();
    }

    public String getNoOfChildrenPassengers(){
        return noOfChildrenPas.getText();
    }

    public String getErrorMessageText(){
        return errorMessageContainer.getText();
    }

    public void selectBusinessClass(){
        classForTravelMenu.click();
        businessClass.click();
    }

}
