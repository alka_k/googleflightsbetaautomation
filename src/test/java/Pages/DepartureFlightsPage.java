package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DepartureFlightsPage {

    public DepartureFlightsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"flt-app\"]/div[2]/div[2]/div[5]/div[1]/div[2]/div[5]/div[2]/ol/li[1]/div/div[1]/div[2]/div[2]")
    public WebElement expandBestFlight;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[2]/div[5]/div[1]/div[2]/div[5]/div[2]/ol/li[1]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]")
    public WebElement selectFlightBestBtn;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[5]/div/div[2]/div[2]/div[3]/div[1]/ol/li[1]/div[3]/span[2]/span[1]")
    public WebElement chooseDepartureToText;

    public String getPageHeaderText() {
        return chooseDepartureToText.getText();
    }
}

