Feature: Below have been been defined varied test cases on the main landing page
  Assertions on changing secondary options have been done
  The primary options have been validated

  Background: The user lands on the required url and waits for it to load completely
    Given I navigate to the main landing page
    And wait for the page to load completely

  Scenario: Check if the return journey option is selected by default when we land on the page
    And I check the default selected option in the dropdown for the ticket type

  Scenario: Check if the return journey date option disappears on selecting one way ticket type
    When I select the one way ticket option from the drop down
    Then The return journey date option should disappear

  Scenario: Check if the add flights option enables on selecting multi city ticket type
    When I select the multi city option from the drop down
    Then Add city button should be enabled
    And The return journey date option should disappear

  Scenario: Check if the from location of the flight search is auto-filled
    And see if the from location has some location already from geolocation

  Scenario: Check if the number of passengers can be reduced after increasing
    When I click on number of passengers drop down
    And I change the number of children passengers to "2"
    Then I click on the minus children passengers button "1" times
    And assert if the number has changed

  Scenario Outline: Check if the correct error message is shown in different cases
    When I click on number of passengers drop down
    And I change the number of adult passengers to "<adult>"
    And I change the number of children passengers to "<children>"
    And I change the number of infant passengers to "<infant>"
    And I change the number of infant on lap passengers to "<infantLap>"
    Then check if the required error message is displayed when "<adult>" "<children>" "<infant>" "<infantLap>"


    Examples:
      |adult|children|infant|infantLap|
      |1    |0       |5     |3        |
      |1    |0       |2     |0        |
      |1    |2       |5     |0        |
      |1    |9       |0     |0        |

  Scenario: Check if a list of flights appear on searching with IATA code
  and the changed departure and arrival dates as required
    When I click on the destination of travel field
    And I enter a random location from IATA list
    And I enter the departure date as "1" month from today
    And I enter the arrival date as "2" weeks later from "1" month from today as the departure date
    Then see if the search button is enabled
    And I click on the search button
    Then assert if the departure page is shown
