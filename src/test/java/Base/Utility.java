package Base;


import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.CsvToBean;
import au.com.bytecode.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

public class Utility {

    public WebDriver driver;

    public static boolean checkIfElementEnabled(WebElement element) {
        try {
            element.isEnabled();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static List<IataCsvPojo> getListFromCSV(String CSVFilePath) {
        try {
            InputStream stream = new FileInputStream(CSVFilePath);
            Reader reader = new InputStreamReader(stream);
            CSVReader csvReader = new CSVReader(reader);

            Map<String, String> mapping = new HashMap<>();
            mapping.put("Location", "location");
            mapping.put("Official AirportWebsite", "website");
            mapping.put("State", "state");
            mapping.put("IATA-Code", "codes");

            HeaderColumnNameTranslateMappingStrategy<IataCsvPojo> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
            strategy.setType(IataCsvPojo.class);
            strategy.setColumnMapping(mapping); //mapping of POJO to the CSV reader
            CsvToBean<IataCsvPojo> csvToBean = new CsvToBean<>();
            List<IataCsvPojo> list = csvToBean.parse(strategy, csvReader);

            return list;
        } catch (Exception e) {

            e.printStackTrace();
        }
        return null;
    }

    public static IataCsvPojo pickRandomFromCSV(List<IataCsvPojo> IATA) { //pick random columns from CSV
        Random rand = new Random();
        IataCsvPojo randomElement = IATA.get(rand.nextInt(IATA.size()));
        return randomElement;
    }

    public static String selectItemFromColumns(String CSVFilePath, String getColumnName) {
        List<IataCsvPojo> listFromCSV = getListFromCSV(CSVFilePath);
        IataCsvPojo randomElement = pickRandomFromCSV(listFromCSV);
        if (getColumnName == "location") {
            return randomElement.getLocation();
        } else if (getColumnName == "state") {
            System.out.println(randomElement.getState());
            return randomElement.getState();
        } else if (getColumnName == "website") {
            System.out.println(randomElement.getWebsite());
            return randomElement.getWebsite();
        } else if (getColumnName == "codes") {
            System.out.println(randomElement.getCodes());
            return randomElement.getCodes();
        }
        return null;
    }

    public static void pressEnterKey() {
        try {
            Robot robot = new Robot();
            robot.delay(200);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.delay(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int convertStringToInt(String textNumber) {
        return Integer.parseInt(textNumber);
    }

    public static int getChangedNoOfPas(int beforeChange, int afterChange) {
        return afterChange - beforeChange;
    }

    public static LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    public static void waitForPageReadyState(WebDriver driver) {
        Wait<WebDriver> wait = new WebDriverWait(driver, 3000);
        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                System.out.println("Current Window State: "
                        + String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
                return String
                        .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                        .equals("complete");
            }
        });

    }

    public static void changeNoOfPassengers(WebDriver driver, int changeToNumber, WebElement elementToBeChanged) {
        Actions actions = new Actions(driver);
        waitForPageReadyState(driver);
        for (int i = 0; i < changeToNumber; i++) {
            waitForPageReadyState(driver);
            actions.moveToElement(elementToBeChanged).click().perform();
        }
    }
}
