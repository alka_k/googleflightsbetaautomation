package Base;


public class IataCsvPojo {

        private String location;
        private String website;
        private String state;
        private String codes;

        public String getLocation() {
            return location;
        }

        public String getWebsite() {
            return website;
        }

        public String getState() {
            return state;
        }

        public String getCodes() {
            return codes;
        }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    @Override
    public String toString() {
        return "IataCsvPojo{" +
                "location='" + location + '\'' +
                ", website='" + website + '\'' +
                ", state='" + state + '\'' +
                ", codes='" + codes + '\'' +
                '}';
    }
}


