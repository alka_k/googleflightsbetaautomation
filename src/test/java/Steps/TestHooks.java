package Steps;

import Base.Utility;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestHooks extends Utility {

    private Utility base;

    public TestHooks(Utility base) {
        this.base = base;
    }

    @Before
    public void initialiseBrowser(){
        System.out.println("Instantiating the browser : Chrome");
        System.setProperty("webdriver.chrome.driver", "src/test/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=en-ca");
        options.addArguments("start-maximized");
        base.driver = new ChromeDriver(options) ;
    }

    @After
    public void TearDownResources(){
        base.driver.quit();
        System.out.println("Closing the browser and all resources instances: Chrome");
    }
}
