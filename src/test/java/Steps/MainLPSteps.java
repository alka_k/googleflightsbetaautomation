package Steps;

import Base.Utility;
import Pages.MainLandingPage;
import cucumber.api.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;


public class MainLPSteps extends Utility {

    private Utility base;
    private static int noOfPasAfterMinus;
    private static int noOfPasAfterPlus;

    public MainLPSteps(Utility base) {
        this.base = base;
    }


    @Given("^I navigate to the main landing page$")
    public void iNavigateToTheMainLandingPage(){
        base.driver.navigate().to("https://www.google.com/flights/beta");
    }

    @And("^I check the default selected option in the dropdown for the ticket type$")
    public void iCheckTheDefaultSelectedOptionInTheDropdownForTheTicketType() throws Throwable {
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        Assert.assertEquals("Round trip",mainLP.ticketTypeMenu.getText());
    }

    @When("^I select the one way ticket option from the drop down$")
    public void iSelectTheOneWayTicketOptionFromTheDropDown() throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        mainLP.selectOneWay();
    }

    @Then("^The return journey date option should disappear$")
    public void theReturnJourneyDateOptionShouldDisappear() throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        Assert.assertEquals(false,mainLP.IsJourneyEndDateActive());
    }

    @When("^I select the multi city option from the drop down$")
    public void iSelectTheMultiCityOptionFromTheDropDown() throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        mainLP.selectMultiCity();
    }

    @Then("^Add city button should be enabled$")
    public void addCityButtonShouldBeEnabled () throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        Assert.assertEquals(true, mainLP.isAddFlightActive());
    }

    @And("^wait for the page to load completely$")
    public void waitForThePageToLoadCompletely() throws Throwable{
        Utility.waitForPageReadyState(base.driver);
    }


    @And("^see if the from location has some location already from geolocation$")
    public void seeIfTheFromLocationHasSomeLocationAlreadyFromGeolocation() throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        Assert.assertEquals(false,mainLP.isFromTravelFilled());
    }

    @And("^I enter a random location from IATA list$")
    public void iEnterARandomLocationFromIATAList() throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        String randomLocation =  Utility.selectItemFromColumns("src/main/resources/IATACodes.csv","codes");

        //Please enter the below details for clarity
        //The column name should be "codes" if you want the IATA code
        // "website" if you want the websites names
        //"state" if the state is required
        // "location" if the airport location is needed

        mainLP.enterTextDestinationTravel(randomLocation);
    }

    @Then("^see if the search button is enabled$")
    public void seeIfTheSearchButtonIsEnabled() throws Throwable{
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        Assert.assertEquals(true,mainLP.isSearchBtnActive());
    }

    @When("^I click on the destination of travel field$")
    public void iClickOnTheDestinationOfTravelField() throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        mainLP.clickDestinationTravel();
    }

    @And("^I press Enter$")
    public void iPressEnter() throws Throwable {
        Utility.waitForPageReadyState(base.driver);
        Utility.pressEnterKey();
    }

    @When("^I click on number of passengers drop down$")
    public void iClickOnNumberOfPassengersDropDown() throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        mainLP.clickNoOfPassengersMenu();
    }

    @Then("^I click on the minus children passengers button \"([^\"]*)\" times$")
    public void iClickOnTheMinusChildrenPassengersButtonTimes(String arg0) throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        Utility.changeNoOfPassengers(base.driver, Utility.convertStringToInt(arg0), mainLP.minusChildrenPas);
        noOfPasAfterMinus=Utility.convertStringToInt(mainLP.getNoOfChildrenPassengers());
    }

    @Then("^assert if the number has changed$")
    public void assertIfTheNumberHasChanged() throws Throwable {
        Assert.assertEquals(true,getChangedNoOfPas(noOfPasAfterMinus,noOfPasAfterPlus)>=0);   //Will always be greater than 0 because we should not
                                                                                                            //be able to reduce the number to smaller than 0
    }

    @And("^I change the number of adult passengers to \"([^\"]*)\"$")
    public void iChangeTheNumberOfAdultPassengersTo(String arg0) throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        Utility.changeNoOfPassengers(base.driver, Utility.convertStringToInt(arg0), mainLP.plusAdultPas);
    }

    @And("^I change the number of children passengers to \"([^\"]*)\"$")
    public void iChangeTheNumberOfChildrenPassengersTo(String arg0) throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        System.out.println("Before : "+mainLP.noOfChildrenPas.getText());
        Utility.changeNoOfPassengers(base.driver, Utility.convertStringToInt(arg0), mainLP.plusChildrenPas);
        noOfPasAfterPlus= Utility.convertStringToInt(mainLP.getNoOfChildrenPassengers());
    }

    @And("^I change the number of infant passengers to \"([^\"]*)\"$")
    public void iChangeTheNumberOfInfantPassengersTo(String arg0) throws Throwable {
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        Utility.changeNoOfPassengers(base.driver, Utility.convertStringToInt(arg0), mainLP.plusInfantPas);
    }


    @And("^I change the number of infant on lap passengers to \"([^\"]*)\"$")
    public void iChangeTheNumberOfInfantOnLapPassengersTo(String arg0) throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        Utility.changeNoOfPassengers(base.driver, Utility.convertStringToInt(arg0), mainLP.plusInfantLapPas);
    }

    @Then("^check if the required error message is displayed when \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void checkIfTheRequiredErrorMessageIsDisplayedWhen(String adult, String children, String infant, String infantLap) throws Throwable {
        System.out.println(" "+adult+" "+children+" "+infant+" "+infantLap);
        MainLandingPage mainLP = new MainLandingPage(base.driver);

        int noOfAdults= Utility.convertStringToInt(adult);
        int totalAdults=noOfAdults+1;  // 1 is the default no of adults
        System.out.println("TotalAdults:"+totalAdults);

        if(totalAdults+ Utility.convertStringToInt(infant)
                + Utility.convertStringToInt(infantLap)
                + Utility.convertStringToInt(children)>9){
            Assert.assertEquals("Sorry, we do not support more than 9 passengers.",mainLP.getErrorMessageText());
        }
        else if(Utility.convertStringToInt(adult)+1< Utility.convertStringToInt(infantLap)){                 //1 adult can take 1 infant on Lap
            Assert.assertEquals("You must have at least one adult per infant in lap.",mainLP.getErrorMessageText());
        }
        else if((Utility.convertStringToInt(adult)+1)*2< Utility.convertStringToInt(infant)){
            Assert.assertEquals("You must have at least one adult per two infants.",mainLP.getErrorMessageText());//1 adult can take 2 infants
        }
        else{
            Assert.assertEquals("",mainLP.getErrorMessageText());
        }
    }

    @And("^I enter the departure date as \"([^\"]*)\" month from today$")
    public void iEnterTheDepartureDateAsMonthFromToday(String months) throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        int monthsToDepart = Utility.convertStringToInt(months);
        LocalDate departure = Utility.getCurrentDate().plus(monthsToDepart, ChronoUnit.MONTHS);
        base.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        ((JavascriptExecutor) base.driver).executeScript("arguments[0].click();", mainLP.journeyStartDate);
        mainLP.enterDepartureDate(departure.toString());
    }

    @And("^I click on the search button$")
    public void iClickOnTheSearchButton() throws Throwable {
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        ((JavascriptExecutor) base.driver).executeScript("arguments[0].click();", mainLP.searchButton);
    }

    @And("^I enter the arrival date as \"([^\"]*)\" weeks later from \"([^\"]*)\" month from today as the departure date$")
    public void iEnterTheArrivalDateAsWeeksLaterFromMonthFromTodayAsTheDepartureDate(String NoOfWeeks, String NoOfMonths) throws Throwable {
        MainLandingPage mainLP= new MainLandingPage(base.driver);
        LocalDate departureDate = Utility.getCurrentDate()
                .plus(Utility.convertStringToInt(NoOfMonths), ChronoUnit.MONTHS);
        LocalDate returnDate = departureDate.plus(Utility
                .convertStringToInt(NoOfWeeks),ChronoUnit.WEEKS);
        mainLP.enterReturnDate(returnDate.toString());
        base.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        mainLP.clickOverlayDoneBtn();
    }


}




