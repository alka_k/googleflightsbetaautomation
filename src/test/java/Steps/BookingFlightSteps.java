package Steps;

import Base.Utility;
import Pages.ArrivalFlightsPage;
import Pages.DepartureFlightsPage;
import Pages.MainLandingPage;
import Pages.TripSummaryPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BookingFlightSteps extends Utility{
    private Utility base;

    public BookingFlightSteps(Utility base) {
        this.base = base;
    }


    @When("^I change the class of travel$")
    public void IChangeTheClassOfTravel() throws Throwable {
        Utility.waitForPageReadyState(base.driver);
        MainLandingPage mainLP = new MainLandingPage(base.driver);
        mainLP.selectBusinessClass();
    }

    @Then("^I click on the first best flight expand button$")
    public void iClickOnTheFirstBestFlightExpandButton() throws Throwable {
        DepartureFlightsPage depart = new DepartureFlightsPage(base.driver);
        Utility.waitForPageReadyState(base.driver);
        WebDriverWait wait = new WebDriverWait(base.driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(depart.expandBestFlight));
        ((JavascriptExecutor) base.driver).executeScript("arguments[0].click();", depart.expandBestFlight);
    }

    @And("^I click on the select flight button$")
    public void iClickOnTheSelectFlightButton() throws Throwable {
        DepartureFlightsPage depart = new DepartureFlightsPage(base.driver);
        ((JavascriptExecutor) base.driver).executeScript("arguments[0].click();", depart.selectFlightBestBtn);
    }


    @Then("^I click on the first returning best flight option$")
    public void iClickOnTheFirstReturningBestFlightOption() throws Throwable {
        Thread.sleep(2000);
        ArrivalFlightsPage arrive = new ArrivalFlightsPage(base.driver);
        WebDriverWait wait = new WebDriverWait(base.driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(arrive.expandArrivalBestFlight.get(0)));
        arrive.expandArrivalBestFlight.get(0).click();
    }

    @Then("^assert if the departure page is shown$")
    public void assertIfTheDeparturePageIsShown() throws Throwable {
        DepartureFlightsPage depart = new DepartureFlightsPage(base.driver);
        Assert.assertEquals(true,depart.getPageHeaderText().contains("Choose departure to "));
    }


    @And("^assert if the trip summary is shown$")
    public void assertIfTheTripSummaryIsShown() throws Throwable {
        TripSummaryPage trip = new TripSummaryPage(base.driver);
        Assert.assertEquals(true,trip.getTripSummaryText().equals("Trip summary"));
    }

}

